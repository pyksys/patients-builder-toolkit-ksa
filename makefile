all:
	yarn install
	yarn build
	[[ -d build ]] && rm -rf build/* || mkdir build
	mv dist build/build
	cp -r deploy_files/* build/

	# need check for account.
	if [ ! -z "$$CI" ]; then heroku builds:create --version="$$CI_COMMIT_MESSAGE" --dir=build -a "$$HEROKU_APP_NAME"; fi
