import Vue from 'vue';
import Component from 'vue-class-component';
const appLang = process.env.VUE_APP_LANG;
const PATIENTS = require('@/data/' + appLang + '/patients.json');

@Component
export class AppDataMixin extends Vue {
  patients = PATIENTS;
}
