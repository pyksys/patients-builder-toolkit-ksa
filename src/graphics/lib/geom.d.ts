//export module geom {}
export function addBarycentricCoordinates(
  bufferGeometry: any,
  removeEdge: boolean
): void;
export function unindexBufferGeometry(bufferGeometry: any): void;
export function addRandomizedAttribute(geometry: any): void;
