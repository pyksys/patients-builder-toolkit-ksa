varying vec3 vBarycentric;
varying float vEven;
varying vec2 vUv;
varying vec3 vPosition;
varying vec3 vRandomy;

uniform float time;
uniform float thickness;
uniform float secondThickness;
uniform float noseVisibility;

uniform float dashRepeats;
uniform float dashLength;
uniform bool dashOverlap;
uniform bool dashEnabled;
uniform bool dashAnimate;

uniform bool seeThrough;
uniform bool insideAltColor;
uniform bool dualStroke;
uniform bool noiseA;
uniform bool noiseB;

uniform bool squeeze;
uniform float squeezeMin;
uniform float squeezeMax;

uniform vec3 stroke;
uniform vec3 fill;
uniform float dotSize;
uniform float timeer;
uniform float huee;

uniform sampler2D wireTiming;

const vec4  kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);
const vec4  kRGBToI     = vec4 (0.596, -0.275, -0.321, 0.0);
const vec4  kRGBToQ     = vec4 (0.212, -0.523, 0.311, 0.0);

const vec4  kYIQToR   = vec4 (1.0, 0.956, 0.621, 0.0);
const vec4  kYIQToG   = vec4 (1.0, -0.272, -0.647, 0.0);
const vec4  kYIQToB   = vec4 (1.0, -1.107, 1.704, 0.0);

vec4 hue_shift(vec4 color, float hue_ofst)
{
    float   YPrime  = dot (color, kRGBToYPrime);
    float   I      = dot (color, kRGBToI);
    float   Q      = dot (color, kRGBToQ);

    // Calculate the chroma
    float   chroma  = sqrt (I * I + Q * Q);

    // hue angel
    float   hue     = atan (Q, I);

    // Convert desired hue back to YIQ
    Q = chroma * sin (hue + hue_ofst);
    I = chroma * cos (hue + hue_ofst);

    // Convert back to RGB
    vec4    yIQ   = vec4 (YPrime, I, Q, 0.0);
    color.r = dot (yIQ, kYIQToR);
    color.g = dot (yIQ, kYIQToG);
    color.b = dot (yIQ, kYIQToB);
	return color;
}

// #pragma glslify: noise = require('glsl-noise/simplex/4d');
//#pragma glslify: PI = require('glsl-pi');
#define PI 3.14159265359


float computeEdge(float pivot, float dir, vec2 msk)
{
  float ofst = msk[0];
  float ignoreMask = msk[1] * noseVisibility;
  float x = pivot;
  float y = dir;
  x = 1.0 - smoothstep(thickness - fwidth(x), thickness + fwidth(x), x);

  float dl = 1.0 - dashLength;
  //dl *= 0.8; // scale down and
  //dl += 0.1; // offset
  float mask = smoothstep(dl - 0.1, dl, ofst );

  x *= clamp(mask+ignoreMask*10.0,0.0, 10.0);
  x = pow(x, mask+1.0);

  return x;
}

float computeEdgeAlways(float pivot, float dir)
{
  float x = pivot;
  float y = dir;
  x = 1.0 - smoothstep(0.01 - fwidth(x), 0.01 + fwidth(x), x);
  return x;
}

vec2 maskof(vec2 uv){
  vec2 msk = 1.0 - texture2D( wireTiming, uv ).rg;
  return msk;
}

// This function returns the fragment color for our styled wireframe effect
// based on the barycentric coordinates for this fragment
vec4 getStyledWireframe (vec3 barycentric) {

  vec2 msk = maskof(vUv);

  float x = barycentric.x;
  float y = barycentric.y;
  float z = barycentric.z;
  float d1 = computeEdge(x, y, msk);
  float d2 = computeEdge(y, z, msk);
  float d3 = computeEdge(z, x, msk);

  float a1 = computeEdgeAlways(x, y);
  float a2 = computeEdgeAlways(y, z);
  float a3 = computeEdgeAlways(z, x);

  float e = max(d1, max(d2, d3));       // streng edges
  //float f = max(a1, max(a2, a3)); // faint always edges
  //float st = max(e, f);
  float st = e;

  vec4 ret = vec4(stroke, st);
  vec4 blue = vec4(0.655,0.973,0.416,st+0.5*sin( timeer ));
  float fst = vRandomy.x;
  fst *= (1.0 - smoothstep(1.7, 2.5, huee));
  ret = hue_shift(ret, huee + fst);
  ret = mix(ret,mix(ret,blue,noseVisibility),msk[1]);
  return ret;
}

void main () {
  gl_FragColor = getStyledWireframe(vBarycentric);
}
