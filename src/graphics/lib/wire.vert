
#include <common>
#include <skinning_pars_vertex>

attribute vec3 barycentric;
attribute vec3 randomy;
attribute float even;

varying vec3 vBarycentric;

varying vec3 vPosition;
varying vec3 vRandomy;
varying float vEven;
varying vec2 vUv;
uniform float timeer;
uniform float huee;

void main () {

  #include <skinbase_vertex>

  #include <begin_vertex>
  #include <skinning_vertex>

  /* gl_Position = projectionMatrix * modelViewMatrix * vec4(position.xyz, 1.0); */
  #include <project_vertex>

  vBarycentric = barycentric;
  vPosition = position.xyz;
  //gl_Position.x += sin((timeer + gl_Position.z * randomy.x ) * 2.0 ) * 10.0;
  vEven = even;
  vUv = uv;
  vRandomy = randomy;
  vec4 wp = modelMatrix * vec4( transformed, 1.0 );

  // Variate randomyx in world X direction
  vRandomy.x = fract(vRandomy.x + huee/2.5);
  vRandomy.x = ((vRandomy.x - 0.5) * 10.0 + 0.5);
  float rgrad = mix(vRandomy.x, (0.0 + (wp.x) ) * 0.15 + 0.5, 0.8);
  rgrad = rgrad * (sin( mix(0.0, 3.14, huee / 2.5) ) );
  vRandomy.x = clamp(rgrad, 0.0, 1.0);
}

