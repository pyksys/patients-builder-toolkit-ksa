// import TWEEN from '@tweenjs/tween.js';
import TWEEN from './lib/tween/tween.esm.js';
// const TWEEN = require('./lib/tween/tween.esm.js');
// const TWEEN = require('@tweenjs/tween.js');
// const TWEEN = require('lib/tween/tween.esm.js');

import * as THREE from 'three';
import {
  Color,
  WebGLRenderer,
  Object3D,
  AnimationMixer,
  AnimationAction,
  Mesh,
  Points,
  ShaderMaterial,
  WebGLRenderTarget,
  Vector3,
  AlphaFormat,
  log,
} from 'three';

const appLang = process.env.VUE_APP_LANG;
const CONTENT = require('@/data/' + appLang + '/content.json');

const controlsData = (CONTENT as any).builder.controlsData;

export interface BioDataParameter {
  title: string;
  subtitle: string;
  value_type?: string;
  min_value: number;
  max_value: number;
  value: number;
  displayMin?: string;
  displayMax?: string;
  type?: object;
  depends: string;
}

export interface BioToggle {
  title: string;
  value: boolean;
  min_value: boolean;
  type: object;
}

export interface BioData {
  [index: string]: BioDataParameter | BioToggle;
  eosinophil: BioDataParameter;
  lge: BioDataParameter;
  lung: BioDataParameter;
  asthma: BioDataParameter;
  ocs: BioToggle;
  aero: BioToggle;
  type2: BioToggle;
  feno: BioDataParameter;
  rhinitis: BioToggle;
  dermatitis: BioToggle;
  polyps: BioToggle;
}

export let data = Object.assign({}, controlsData);

export function resetValues(data: any) {
  for (const i in data) {
    if (data[i]) {
      data[i].value = data[i].min_value;
    }
  }
}

export function normalizeData(param: BioDataParameter) {
  const range = param.max_value - param.min_value;
  const v = (param.value - param.min_value) / range;
  return v;
}

export class Setup {
  public pointsObject: Points | undefined;
  public sphere: Mesh | undefined;
  public bg: Mesh | undefined;
  public body: Mesh | undefined;
  public wireframe: Object3D | undefined;
  public target: WebGLRenderTarget;
  public renderer: WebGLRenderer;
  public mixer: AnimationMixer | undefined;
  public action: AnimationAction | undefined;
  public action2: AnimationAction | undefined;
  public action3: AnimationAction | undefined;
  public action4: AnimationAction | undefined;
  public scene = new THREE.Scene();
  public camera = (() => {
    const c = new THREE.PerspectiveCamera(60, 1.5, 50, 5000);
    c.setFocalLength(250);
    c.updateProjectionMatrix();
    return c;
  })();
  public clock = new THREE.Clock();
  public emitter!: any;
  public guiData!: any;
  public bgImage!: THREE.Mesh;
  public bodyMat!: THREE.ShaderMaterial;
  public wireMat!: THREE.ShaderMaterial;
  public bgMat!: THREE.ShaderMaterial;
  public innerPartsMtl!: THREE.ShaderMaterial;
  public headPartsMtl!: THREE.ShaderMaterial;
  public neckLeftPartsMtl!: THREE.ShaderMaterial;
  public neckRightPartsMtl!: THREE.ShaderMaterial;
  public chestPartsMtl!: THREE.ShaderMaterial;
  public vertexPartsMtl!: THREE.ShaderMaterial;
  public bgAmplitude: number = 0.2;
  public bgAmplitudeMin: number = 0.1;
  public bgFreqMin: number = 1.0;
  public bgFreq: number = 8.0;

  constructor(canvas: HTMLCanvasElement) {
    this.renderer = new WebGLRenderer({
      canvas,
      antialias: true,
    });
    // this.renderer.autoClearDepth = true;
    // this.renderer.state.buffers.depth.setClear( 0.0 );
    this.renderer.sortObjects = false;
    // this.renderer.setPixelRatio( window.devicePixelRatio );
    // this.renderer.setSize(window.innerWidth, window.innerHeight, true);

    // Create a multi render target with Float buffers
    // const target = new THREE.WebGLRenderTarget( window.innerWidth, window.innerHeight );
    const target = new THREE.WebGLRenderTarget(1024, 1024);
    target.texture.format = THREE.RGBFormat;
    target.texture.minFilter = THREE.NearestFilter;
    target.texture.magFilter = THREE.NearestFilter;
    target.texture.generateMipmaps = false;
    target.stencilBuffer = false;
    target.depthBuffer = true;
    // target.depthTexture = new THREE.DepthTexture(window.innerWidth, window.innerHeight);
    target.depthTexture = new THREE.DepthTexture(512, 512);
    target.depthTexture.type = THREE.UnsignedShortType;
    // target.setSize(1024, 768);
    this.target = target;
  }
}

// tslint:disable-next-line: max-classes-per-file
export class Handlers {
  public dramaticValue: number = 0.0;
  private setup: Setup;
  private lastClip!: THREE.AnimationAction;
  private state: number = 0;

  constructor(setup: Setup) {
    this.setup = setup;
  }

  public initialize(animated: boolean) {
    if (!animated) {
      for (const i in data) {
        if (i in this) {
          (this as any)[i]((data as any)[i].min_value);
        }
      }
    } else {
      for (const i in data) {

        if (data[i]) {
          if (data[i].type === 'slider') {
            const d = { value: data[i].value };
            const tween = new (TWEEN as any).Tween(d)
              .to({ value: data[i].min_value }, 500)
              .easing((TWEEN as any).Easing.Quadratic.Out)
              .onUpdate(() => {
                (this as any)[i](d.value);
              })
              .start();
          }
          else {
            (this as any)[i](data[i].min_value)
          }
        }
      }
    }
  }

  public enterIdle(): void {
    data.asthma.value = data.asthma.min_value;
    this.asthma(data.asthma.value);
    this.valueChanged(0);

    const d = { value: data.lung.value };
    const tween = new (TWEEN as any).Tween(d)
      .to({ value: data.lung.min_value }, 2000)
      .easing((TWEEN as any).Easing.Quadratic.Out)
      .onUpdate(() => {
        data.lung.value = d.value;
        this.lung(data.lung.value);
      })
      .start();
  }

  public valueChanged(v: number) {
    this.dramaticValue = this.calculateDramaticValue();

    const frequency =
      this.setup.bgFreq * this.dramaticValue + this.setup.bgFreqMin; // target
    const curFrequency = this.setup.bgMat.uniforms.freq.value; // current

    const phase = this.setup.bgMat.uniforms.phase.value; // current phase
    const time = this.setup.bgMat.uniforms.time.value; // current time

    const curr = (time * curFrequency + phase) % (2.0 * Math.PI);
    const next = (time * frequency) % (2.0 * Math.PI);

    this.setup.bgMat.uniforms.phase.value = curr - next; // set different
    this.setup.bgMat.uniforms.freq.value = frequency; // net teraget freq
  }

  public feno(v: number) {
    const max = data.feno.max_value;
    const min = data.feno.min_value;
    const range = max - min;
    const value = (v - min) / range;
    // this.setup.innerPartsMtl.uniforms.coef.value = 1.0 - value;

    if (this.setup.innerPartsMtl) {
      this.setup.innerPartsMtl.uniforms.multiplier.value = value + 0.3;
    }
    // const nv = new Vector3();
    // nv.copy(this.setup.cameraPositions[0]);
    // const na = nv.lerp(this.setup.cameraPositions[1]);
    // this.setup.camera.position = na;

  }

  // tslint:disable-next-line: no-empty
  public eosinophil(v: number) {
    const max = data.eosinophil.max_value;
    const min = data.eosinophil.min_value;
    const range = max - min;
    const value = (v - min) / range;

    if (this.setup.body) {
      const mat = this.setup.bodyMat;
      const maxH = 2.5;
      if (mat) {
        mat.uniforms.huee.value = THREE.MathUtils.lerp(0.0, maxH, value);
        this.setup.vertexPartsMtl.uniforms.huee.value = THREE.MathUtils.lerp(
          0.0,
          maxH,
          value
        );
        // mat.uniforms.mesh_opacity.value = THREE.MathUtils.lerp(0.5, 0.90, value);
        mat.uniforms.mesh_definition.value = THREE.MathUtils.lerp(
          0.01,
          0.2,
          value
        );
      }
      const mat2 = this.setup.wireMat;
      if (mat2) {
        mat2.uniforms.huee.value = THREE.MathUtils.lerp(0.0, maxH, value);
      }
    }
  }

  public lge(v: number) {
    const max = data.lge.max_value;
    const min = data.lge.min_value;
    const range = max - min;
    const value = (v - min) / range;
    if (this.setup.body) {
      const mat = this.setup.wireMat;
      if (mat) {
        mat.uniforms.dashLength.value = THREE.MathUtils.clamp(
          0.05 + value,
          0.0,
          1.0
        );
        mat.uniforms.dotSize.value = THREE.MathUtils.lerp(0.12, 0.7, value);
      }
    }
  }

  public lung(v: number) {
    const max = data.lung.max_value;
    const min = data.lung.min_value;
    const range = max - min;
    const value = (v - min) / range;

    if (this.setup.mixer) {
      this.setup.mixer.timeScale = 1.0 + value * 0.4;
    }

    if (
      this.setup.action &&
      this.setup.action2 &&
      this.setup.action3 &&
      value <= 0.5
    ) {
      const v1 = value * 2.0;
      this.setup.action.setEffectiveWeight(1.0 - v1);
      this.setup.action2.setEffectiveWeight(v1);
      this.setup.action3.setEffectiveWeight(0);
    }

    // if (!this.lastClip) {
    //     this.setup.action.setEffectiveWeight(1.0);
    //     this.setup.action2.setEffectiveWeight(1.0);
    //     this.setup.action3.setEffectiveWeight(1.0);
    //     this.lastClip = this.setup.action;
    // }

    // if (this.lastClip != this.setup.action && this.setup.action && this.setup.action2
    //     && this.setup.action3 && value < 0.3) {
    //     const v1 = value * 2.0;
    //     this.lastClip.crossFadeTo(this.setup.action, 1.0, false);
    //     this.lastClip = this.setup.action;
    //     // this.setup.action2.crossFadeTo(this.setup.action, 1.0, false);
    //     // this.setup.action.setEffectiveWeight(1.0);
    //     // this.setup.action2.setEffectiveWeight(0.0);
    //     // this.setup.action3.setEffectiveWeight(0.0);
    // }

    // if (this.lastClip != this.setup.action2 && this.setup.action && this.setup.action2
    //     && this.setup.action3 && value >= 0.3 && value < 0.6) {
    //     const v1 = value * 2.0;
    //     this.lastClip.crossFadeTo(this.setup.action2, 1.0, false);
    //     this.lastClip = this.setup.action2;
    //     // this.setup.action.setEffectiveWeight(0.0);
    //     // this.setup.action2.setEffectiveWeight(1.0);
    //     // this.setup.action3.setEffectiveWeight(0.0);
    // }

    // if (this.lastClip != this.setup.action3 && this.setup.action && this.setup.action2 && value >= 0.6
    //     && this.setup.action3 ) {
    //     const v1 = value * 2.0;
    //     this.lastClip.crossFadeTo(this.setup.action3, 1.0, false);
    //     this.lastClip = this.setup.action3;
    //     // this.setup.action.setEffectiveWeight(0.0);
    //     // this.setup.action2.setEffectiveWeight(0.0);
    //     // this.setup.action3.setEffectiveWeight(1.0);
    // }

    if (
      this.setup.action &&
      this.setup.action2 &&
      this.setup.action3 &&
      value > 0.5
    ) {
      const v2 = (value - 0.5) * 2.0;
      this.setup.action.setEffectiveWeight(0);
      this.setup.action2.setEffectiveWeight(1.0 - v2);
      this.setup.action3.setEffectiveWeight(v2);
    }
  }

  // tslint:disable-next-line: no-empty
  public asthma(v: number) {
    const max = data.asthma.max_value;
    const min = data.asthma.min_value;
    const range = max - min;
    const value = ((v - min) / range);

    this.setup.bgMat.uniforms.amplitude.value =
      this.setup.bgAmplitude * value + this.setup.bgAmplitudeMin;

    if (this.setup.bg) {
      if (!this.setup.bg.morphTargetInfluences) {
         this.setup.bg.morphTargetInfluences = [value];
      } else {
         this.setup.bg.morphTargetInfluences[0] = 0.5;
         this.setup.bg.morphTargetInfluences[0] = value;
      }
    }
  }

  public ocs(v: boolean) {
    const mtl = this.setup.vertexPartsMtl;
    mtl.uniforms.ocs_disable.value = v ? 0.0 : 1.0;

    const d = { value: mtl.uniforms.size.value };
    const tween = new (TWEEN as any).Tween(d)
      .to({ value: v ? 25.0 : 15.0 }, 1000)
      .easing((TWEEN as any).Easing.Quadratic.Out)
      .onUpdate(() => {
        mtl.uniforms.size.value = d.value;
      })
      .start();
  }

  public aero(v: boolean) {
    const mtl = this.setup.headPartsMtl;
    //mtl.uniforms.ocs_disable.value = v ? 0.0 : 1.0;

    const d = { value: mtl.uniforms.size.value };
    const tween = new (TWEEN as any).Tween(d)
      .to({ value: v ? 10.0 : 0.0 }, 1000)
      .easing((TWEEN as any).Easing.Quadratic.Out)
      .onUpdate(() => {
        mtl.uniforms.size.value = d.value;
      })
      .start();
  }

  public type2(v: boolean) {
    this.rhinitis(false)
    this.dermatitis(false)
    this.polyps(false)
  }

  public rhinitis(v: boolean) {
    const mtl = this.setup.chestPartsMtl;
    //mtl.uniforms.ocs_disable.value = v ? 0.0 : 1.0;

    const d = { value: mtl.uniforms.size.value };
    const tween = new (TWEEN as any).Tween(d)
      .to({ value: v ? 10.0 : 0.0 }, 1000)
      .easing((TWEEN as any).Easing.Quadratic.Out)
      .onUpdate(() => {
        mtl.uniforms.size.value = d.value;
      })
      .start();
  }

  public dermatitis(v: boolean) {
    const mtl = this.setup.neckLeftPartsMtl;
    //mtl.uniforms.ocs_disable.value = v ? 0.0 : 1.0;

    const d = { value: mtl.uniforms.size.value };
    const tween = new (TWEEN as any).Tween(d)
      .to({ value: v ? 10.0 : 0.0 }, 1000)
      .easing((TWEEN as any).Easing.Quadratic.Out)
      .onUpdate(() => {
        mtl.uniforms.size.value = d.value;
      })
      .start();
  }

  public polyps(v: boolean) {
    this.setup.wireMat.uniforms.noseVisibility.value = v ? 1.0 : 0.0;
  }

  private calculateDramaticValue() {
    return normalizeData(data.asthma);
    let d = 0.0;
    const count = 5;

    for (const i in data) {
      if (data[i]) {
        const param = (data as any)[i];
        if (!param.type) {
          const value = normalizeData(param);
          d += value / count;
        } else {
          d += data[i].value ? 1.0 / count : 0.0;
        }
      }
    }
    return d;
  }
}
