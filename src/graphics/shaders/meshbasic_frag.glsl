uniform vec3 diffuse;
uniform float opacity;

#ifndef FLAT_SHADED

	varying vec3 vNormal;

#endif

#include <common>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <uv2_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <envmap_pars_fragment>
#include <fog_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>

varying vec3 b_vWorldPosition;
varying vec3 b_vCameraToVertex;
varying vec3 b_vWorldNormal;
varying float b_fresnel;
varying vec3 vRandomy;

uniform vec3 fresnelColor;
uniform float mesh_opacity;
uniform float mesh_definition;
uniform vec3 baseColor;
uniform vec3 tintColor;
uniform vec3 orangeColor;
uniform float timeer;
uniform float huee;

const vec4  kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);
const vec4  kRGBToI     = vec4 (0.596, -0.275, -0.321, 0.0);
const vec4  kRGBToQ     = vec4 (0.212, -0.523, 0.311, 0.0);

const vec4  kYIQToR   = vec4 (1.0, 0.956, 0.621, 0.0);
const vec4  kYIQToG   = vec4 (1.0, -0.272, -0.647, 0.0);
const vec4  kYIQToB   = vec4 (1.0, -1.107, 1.704, 0.0);

vec4 hue_shift(vec4 color, float hue_ofst)
{
    float   YPrime  = dot (color, kRGBToYPrime);
    float   I      = dot (color, kRGBToI);
    float   Q      = dot (color, kRGBToQ);

    // Calculate the chroma
    float   chroma  = sqrt (I * I + Q * Q);

    // hue angel
    float   hue     = atan (Q, I);

    // Convert desired hue back to YIQ
    Q = chroma * sin (hue + hue_ofst);
    I = chroma * cos (hue + hue_ofst);

    // Convert back to RGB
    vec4    yIQ   = vec4 (YPrime, I, Q, 0.0);
    color.r = dot (yIQ, kYIQToR);
    color.g = dot (yIQ, kYIQToG);
    color.b = dot (yIQ, kYIQToB);
	return color;
}

void main() {

	#include <clipping_planes_fragment>

	vec4 diffuseColor = vec4( diffuse, opacity );

	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <specularmap_fragment>

	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );

	// accumulation (baked indirect lighting only)
	#ifdef USE_LIGHTMAP

		reflectedLight.indirectDiffuse += texture2D( lightMap, vUv2 ).xyz * lightMapIntensity;

	#else

		reflectedLight.indirectDiffuse += vec3( 1.0 );

	#endif

	// modulation
	#include <aomap_fragment>

	reflectedLight.indirectDiffuse *= diffuseColor.rgb;

	vec3 outgoingLight = reflectedLight.indirectDiffuse;

	#include <envmap_fragment>

	//gl_FragColor = vec4( outgoingLight, diffuseColor.a );

	#include <premultiplied_alpha_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>

	//gl_FragColor = vec4( diffuseColor.rgb, diffuseColor.a );
	// gl_FragColor = vec4( diffuseColor.rgb, dot(b_vWorldNormal, b_vCameraToVertex));
	// gl_FragColor = vec4( b_vWorldNormal, 1);
	// gl_FragColor = vec4( b_vWorldPosition * 0.2, 1);
	// float fresnel = dot(b_vWorldNormal, b_vCameraToVertex);
	// fresnel = saturate(fresnel);
	// fresnel = 0.0 + 1.0 * pow(1.0 + fresnel, 2.0f);
	//gl_FragColor = vec4( gl_FragColor.rgb + baseColor.rgb * b_fresnel, b_fresnel + mesh_opacity);
	// gl_FragColor = vec4( gl_FragColor.rgb + baseColor.rgb * b_fresnel, b_fresnel + mesh_opacity);

	// just texture unlit
    vec4 tcc = texture2D( map, vUv );

    // variate by randomy
  	float tt = sin(1.5 * timeer + vRandomy.z * 3.14 * 2.0) * 0.5 + 0.5;
	// vec3 brighter = baseColor;
	// vec3 brighter = clamp(tcc.rgb + 0.2, 0.0, 1.0);
	// tcc.rgb = mix( tcc.rgb, brighter, tt );

	// clamp with base color
	tcc.rgb = mix(baseColor, tcc.rgb, mesh_definition );

	tcc.w = clamp(mesh_opacity + tt * 0.05, 0.0, 1.0);
    tcc = mix(tcc, vec4(fresnelColor, 1.0), b_fresnel );

	tcc.rgb *= tintColor;
	float fst = vRandomy.x;
	fst *= (1.0 - smoothstep(1.7, 2.5, huee));
	tcc = hue_shift(tcc, huee + fst);
	float factor = smoothstep(0.3, 0.5, huee / 5.0);
	tcc.rgb = mix(tcc.rgb, tcc.rgb + orangeColor - 0.5, factor);
    // tcc = vec4(b_fresnel, b_fresnel, b_fresnel, 1.0);
	gl_FragColor = tcc;
	// gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}
