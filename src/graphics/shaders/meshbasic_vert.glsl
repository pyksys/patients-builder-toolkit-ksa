#include <common>
#include <uv_pars_vertex>
#include <uv2_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>

attribute vec3 randomy;

varying vec3 b_vWorldPosition;
varying vec3 b_vCameraToVertex;
varying vec3 b_vWorldNormal;
varying vec3 vRandomy;
varying float b_fresnel;

uniform float fresnelCoeff;
uniform float fresnelPow;
uniform float fresnelThresh;
uniform float fresnelCutoff;
uniform float huee;
uniform float timeer;
uniform float mesh_definition;

void main() {

	#include <uv_vertex>
	#include <uv2_vertex>
	#include <color_vertex>
	#include <skinbase_vertex>

	#ifdef USE_ENVMAP

	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>

	#endif

	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>

	#include <worldpos_vertex>
	#include <clipping_planes_vertex>
	#include <envmap_vertex>
	#include <fog_vertex>

	vec4 wp = modelMatrix * vec4( transformed, 1.0 );
	b_vWorldPosition = wp.xyz;
	b_vCameraToVertex = normalize( wp.xyz - cameraPosition );

	vec3 b_objectNormal = vec3( normal );
	#ifdef USE_TANGENT
		vec3 b_objectTangent = vec3( tangent.xyz );
	#endif
	#ifdef USE_MORPHNORMALS
		b_objectNormal += ( morphNormal0 - normal ) * morphTargetInfluences[ 0 ];
		b_objectNormal += ( morphNormal1 - normal ) * morphTargetInfluences[ 1 ];
		b_objectNormal += ( morphNormal2 - normal ) * morphTargetInfluences[ 2 ];
		b_objectNormal += ( morphNormal3 - normal ) * morphTargetInfluences[ 3 ];
	#endif
	#ifdef USE_SKINNING
		mat4 b_skinMatrix = mat4( 0.0 );
		b_skinMatrix += skinWeight.x * boneMatX;
		b_skinMatrix += skinWeight.y * boneMatY;
		b_skinMatrix += skinWeight.z * boneMatZ;
		b_skinMatrix += skinWeight.w * boneMatW;
		b_skinMatrix = bindMatrixInverse * b_skinMatrix * bindMatrix;
		b_objectNormal = vec4( b_skinMatrix * vec4( b_objectNormal, 0.0 ) ).xyz;
		#ifdef USE_TANGENT
			b_objectTangent = vec4( b_skinMatrix * vec4( b_objectTangent, 0.0 ) ).xyz;
		#endif
	#endif
	vec3 b_transformedNormal = normalMatrix * b_objectNormal;
	#ifdef FLIP_SIDED
		b_transformedNormal = - transformedNormal;
	#endif
	#ifdef USE_TANGENT
		vec3 b_transformedTangent = normalMatrix * b_objectTangent;
		#ifdef FLIP_SIDED
			b_transformedTangent = - b_transformedTangent;
		#endif
	#endif
        
	b_vWorldNormal = normalize(inverseTransformDirection( b_transformedNormal, viewMatrix )); 

	float fresnel = dot(b_vWorldNormal, b_vCameraToVertex);
	fresnel = fresnelCoeff * pow(1.0 + fresnel, fresnelPow);
	fresnel = smoothstep(fresnelCutoff - fresnelThresh, fresnelCutoff + fresnelThresh, fresnel);
	b_fresnel = fresnel;

	vRandomy = randomy;

    // Variate randomyx in world X direction
	vRandomy.x = fract(vRandomy.x + huee/2.5);
	vRandomy.x = ((vRandomy.x - 0.5) * 10.0 + 0.5);
	float rgrad = mix(vRandomy.x, (0.0 + (b_vWorldPosition.x ) ) * 0.15 + 0.5, 0.8);
	rgrad = rgrad * (sin( mix(0.0, 3.14, huee / 2.5) ) );
	vRandomy.x = clamp(rgrad, 0.0, 1.0);

    //gl_Position.x += sin((timeer + gl_Position.z * randomy.x ) * 2.0 ) * 20.0 * (0.8-mesh_definition) ;
    // gl_Position.xyz -= b_transformedNormal * (0.5 + 0.5 * sin((timeer + randomy.x ) * 2.0 ) ) * 20.0 * (0.8-mesh_definition) ;
	// vec3 pos = gl_Position.xyz / gl_Position.w;
    // pos += b_transformedNormal * (mesh_definition - 0.5);
	// gl_Position.xyz = pos;
	// gl_Position.w = 1.0;
}
