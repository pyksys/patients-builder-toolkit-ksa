process.env.VUE_APP_VERSION =
  process.env.APP_VERSION || require('./package.json').version;
process.env.VUE_APP_LANG = process.env.APP_LANG || 'en';
module.exports = {
  publicPath: './',
  css: {
    sourceMap: process.env.NODE_ENV === 'development',
    loaderOptions: {
      sass: {
        prependData: `@import "@/assets/styles/prependData.scss";`,
      },
    },
  },
  chainWebpack: (config) => {
    ['vue-modules', 'vue', 'normal-modules', 'normal'].forEach((oneOf) =>
      config.module
        .rule('scss')
        .oneOf(oneOf)
        .use('sass-loader')
        .tap((options) => ({
          ...options,
          prependData: `@import "@/assets/styles/prependData.scss";`,
        }))
    );
    config.module
      .rule('glsl')
      .test(/\.glsl$/)
      .use('raw-loader')
      .loader('raw-loader')
      .end();

    /*config.module
      .rule('png')
      .test(/\.(png)$/)
      .use('file-loader')
      .loader('file-loader')
      .end();*/

    const imagesRule = config.module.rule('images');
    imagesRule.uses.clear();

    imagesRule.use('file-loader').loader('file-loader');
  },
};
